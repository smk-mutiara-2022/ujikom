@extends('layouts.pembimbing.dashboard')

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-12" style="margin-left: 50px">
                <table class="table table-striped table-responsive text-center">
                    <thead class="">
                        <tr >
                            <th class="text-center">NIS</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center">Nama Pembimbing</th>
                            <th class="text-center">Progress</th>
                            <th class="text-center">Penyusunan Laporan</th>
                            <th class="text-center">aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                                <tr>
                                    <td scope="row">{{ $item->nis }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        @if ($item->pembimbing_id == null)
                                            Siswa Belum Memiliki Pembimbing
                                        @else
                                            {{ $item->pembimbing->name }}
                                        @endif
                                    </td>
                                    <td>
                                    @if ($item->status == null)
                                        Siswa belum memulai progress
                                    @else
                                        {{ $item->status }}
                                        
                                    @endif
                                    </td>
                                    <td>
                                    @if ($item->laporan == null)
                                        Siswa belum menyusun Laporan
                                    @else
                                        <a href="/download/{{ $item->laporan->file_laporan }}" class="btn btn-outline-primary">Download Laporan</a>
                                        
                                    @endif
                                    </td>
                                    <td>
                                        <a href="/detail/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                <button onclick="kembali()" class="btn btn-danger mb-5">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>
            </div>
        </div>
            
        
     </div>
@endsection